ARG NVIDIA_GPU=no

###################################################################################################################################################
# BASE DEV STAGE
FROM nsdn/ros-foxy-lma:v1 as base-development-stage
LABEL Name="base-development" Version=3.0
LABEL maintainer "Natasha Nakashima <natashasayuri@gmail.com>"

# Some additional packages
RUN apt-get -y update \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install --no-install-recommends \
    git \
    qt5-default \
    qtbase5-dev \
    qtbase5-dev-tools \
    libqwt-qt5-6 \
    libqwt-qt5-dev \
    liburdfdom-tools \
    libpcap-dev \
    curl \
    wget \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get -y update \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install --no-install-recommends \
    pkg-config \
    libglvnd-dev \
    libgl1-mesa-dev \
    libegl1-mesa-dev \
    libgles2-mesa-dev \
    && rm -rf /var/lib/apt/lists/*

# Dependencies for glvnd and X11.
RUN apt-get -y update \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install --no-install-recommends \
    libglvnd0 \
    libgl1 \
    libglx0 \
    libegl1 \
    libxext6 \
    libx11-6 \
    && rm -rf /var/lib/apt/lists/*

###################################################################################################################################################
# NVIDIA DEV STAGE
FROM base-development-stage as development-stage-nvidia-yes
LABEL Name="development-nvidia-yes" Version=3.0

# ARG CUDA_MAJOR=11
# ARG CUDA_MINOR=1

# ENV CUDA_VERSION ${CUDA_MAJOR}.${CUDA_MINOR}
# ENV NCCL_VERSION 2.8.4

# RUN apt-get -y update \
#     && export DEBIAN_FRONTEND=noninteractive \
#     && apt-get -y install --no-install-recommends \
#     gnupg2 \
#     curl \
#     ca-certificates && \
#     curl -fsSL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub | apt-key add - && \
#     echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64 /" > /etc/apt/sources.list.d/cuda.list && \
#     echo "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu2004/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list && \
#     apt-get purge --autoremove -y curl \
#     && rm -rf /var/lib/apt/lists/*

# # For libraries in the cuda-compat-* package: https://docs.nvidia.com/cuda/eula/index.html#attachment-a
# RUN apt-get -y update \
#     && export DEBIAN_FRONTEND=noninteractive \
#     && apt-get -y install --no-install-recommends \
#     cuda-cudart-${CUDA_MAJOR}-${CUDA_MINOR} \
#     cuda-toolkit-${CUDA_MAJOR}-${CUDA_MINOR} \
#     cuda-compat-${CUDA_MAJOR}-${CUDA_MINOR} \
#     && ln -s cuda-${CUDA_MAJOR}.${CUDA_MINOR} /usr/local/cuda && \
#     rm -rf /var/lib/apt/lists/*

# # Required for nvidia-docker v1
# RUN echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf \
#     && echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf

# ENV PATH /usr/local/nvidia/bin:/usr/local/cuda/bin:${PATH}
# ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,video,utility,graphics,display
ENV NVIDIA_REQUIRE_CUDA "cuda>=11.2 brand=tesla,driver>=418,driver<419 brand=tesla,driver>=440,driver<441 driver>=450"

# RUN apt-get -y update \
#     && export DEBIAN_FRONTEND=noninteractive \
#     && apt-get -y install --no-install-recommends \
#     libtinfo5 \
#     libncursesw5 \
#     cuda-cudart-dev-${CUDA_MAJOR}-${CUDA_MINOR} \
#     cuda-command-line-tools-${CUDA_MAJOR}-${CUDA_MINOR} \
#     cuda-minimal-build-${CUDA_MAJOR}-${CUDA_MINOR} \
#     cuda-libraries-dev-${CUDA_MAJOR}-${CUDA_MINOR} \
#     cuda-nvml-dev-${CUDA_MAJOR}-${CUDA_MINOR} \
#     libnpp-dev-${CUDA_MAJOR}-${CUDA_MINOR} \
#     libnccl-dev \
#     libcublas-dev-${CUDA_MAJOR}-${CUDA_MINOR} \
#     libcusparse-dev-${CUDA_MAJOR}-${CUDA_MINOR} \
#     && rm -rf /var/lib/apt/lists/*

# # apt from auto upgrading the cublas package. See https://gitlab.com/nvidia/container-images/cuda/-/issues/88
# RUN apt-mark hold libcublas-dev-${CUDA_MAJOR}-${CUDA_MINOR} libnccl-dev
# ENV LIBRARY_PATH /usr/local/cuda/lib64/stubs


###################################################################################################################################################
# NO NVIDIA DEV STAGE
FROM base-development-stage as development-stage-nvidia-no
LABEL Name="development-nvidia-no" Version=3.0


###################################################################################################################################################
# DEVELOPMENT STAGE
FROM development-stage-nvidia-${NVIDIA_GPU} as development-stage
LABEL Name="development-stage" Version=3.0

# RUN export DEBIAN_FRONTEND=noninteractive \
#     && apt-get -y install --no-install-recommends \
#     curl \
#     gnupg2 \
#     lsb-release

# RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add - \
#     && sh -c 'echo "deb [arch=$(dpkg --print-architecture)] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list'

# Install Python
RUN apt-get -y update \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install --no-install-recommends \
    python3 \
    python3-pip \
    python3-venv \
    && rm -rf /var/lib/apt/lists/*

RUN pip3 install pyserial

# User configuration
ARG USER_NAME=ros
ARG USER_PASSWORD=ros
ARG USER_UID=1000
ARG USER_GID=${USER_UID}

RUN groupadd -g ${USER_GID} ${USER_NAME}

RUN useradd -d /home/${USER_NAME} -s /bin/bash \
    -m ${USER_NAME} -u ${USER_UID} -g ${USER_GID} -G sudo \
    && echo "${USER_NAME}:${USER_PASSWORD}" | \
    chpasswd && adduser ${USER_NAME} sudo

# Groups to access devices
RUN usermod -a -G video ${USER_NAME}
RUN usermod -a -G dialout ${USER_NAME}
# RUN usermod -a -G input ${USER_NAME}

# ROS packages
RUN apt-get -y update \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install --no-install-recommends \
    ros-${ROS_DISTRO}-rviz2 \
    ~nros-${ROS_DISTRO}-rqt* \
    ros-${ROS_DISTRO}-robot-state-publisher \
    ros-${ROS_DISTRO}-example-interfaces \
    ros-${ROS_DISTRO}-tf2-tools \
    ros-${ROS_DISTRO}-diagnostic-updater \
    ros-${ROS_DISTRO}-fastrtps* \
    ros-${ROS_DISTRO}-joy \
    ros-${ROS_DISTRO}-joy-linux \
    ros-${ROS_DISTRO}-joy-teleop \
    # tf2-tools \
    ros-${ROS_DISTRO}-slam-toolbox \ 
    ros-${ROS_DISTRO}-ros2bag \
    ros-${ROS_DISTRO}-rosbag2-converter-default-plugins \
    ros-${ROS_DISTRO}-rosbag2-storage-default-plugins \
    # ros-${ROS_DISTRO}-navigation2 \
    ros-${ROS_DISTRO}-nav2-bringup \
    && rm -rf /var/lib/apt/lists/*

# # [Optional] Uncomment this section to install additional OS packages.
RUN apt-get -y update \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install --no-install-recommends \
    doxygen \
    ros-foxy-pcl-ros \
    ros-foxy-robot-localization \
    python3-vcstool \
    && rm -rf /var/lib/apt/lists/*

RUN groupadd -f -r -g 999 gpio
RUN usermod -a -G gpio ${USER_NAME}


COPY ros_dev_entrypoint_v1.sh ./
USER ${USER_NAME}
ENTRYPOINT [ "/ros_dev_entrypoint_v1.sh" ]
CMD [ "bash" ]

