# Samsung Docker

Files to create a complete Docker enviroment with Ubuntu 20.04, ROS2 foxy and Gazebo for the Scooby Project.

Samsung Docker environment works on:

* Ubuntu Focal (20.04)

* Wndows 10 with WSL2 and Docker

## Install

Install instructions.

1. Install Docker

    Linux instructions [here](https://docs.docker.com/engine/install/ubuntu/).

    Windows WSL2 instructions [here](https://docs.docker.com/docker-for-windows/wsl/).

Before installing Windows Docker Desktop backend, follow the instructions to enable the WLS2 [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

2. Install Docker Compose

    Linux instructions [here](https://docs.docker.com/compose/install/)

Select the installation accordingly to the Operating System you are using.

3. If you have a NVIDIA graphic card, follow the instructions below to install docker nvidia runtime:

    NVIDIA Container Toolkit install guide [here] (https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html)

    Adding the NVIDIA Runtime to Docker [here] (https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/user-guide.html)

All the commands you need have been summarized in 3 scripts to run in Linux terminal. Refer to nvidia-docker.rar file in this project.

Windows WSL2 has no support to NVIDIA Container Toolkit yet.

4. Clone scooby_docker:
        
        cd ~/
        git clone https://nat-sayuri@bitbucket.org/nat-sayuri/scooby_docker.git 

4. Clone scooby_simulation Project and and as submodules to scooby_docker:

        cd ~/scooby_docker/scooby_ws
        mkdir src && cd src
        git submodule add https://olmerg@bitbucket.org/olmerg/scooby_simulation.git

5. If you use Visual Studio Code and Remote-Containers, open scooby_docker as a workspace and select Reopen in Container option. This will compile and create a development container of the project.

More information about Visual Studio Code and Remote-Containers can be found [here](https://code.visualstudio.com/docs/remote/containers)

6. To create a container for the project without Visual Studio Code, you need to change some parameters inside docker-compose.yml

        cd ~/scooby_docker/.devcontainer

        Open the docker-compose.yml file with your preferred text editor and change the volume device option with the absolute path to the scooby_ws directory inside this project.

7. Start project using the Docker Compose file:

        cd ~/scooby_docker/.devcontainer
        docker-compose up --build

6. Start a terminal inside the container:
        
        docker exec -it scooby-development bash

7. Build and run Scooby Project inside container:

        colcon build
        . install setup.sh
        ros2 launch scooby_gazebo scooby_default.launch.py