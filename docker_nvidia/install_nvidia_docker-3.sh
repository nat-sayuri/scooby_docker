#########################################
# Install NVIDIA Container Runtime
#########################################

echo "Installing NVIDIA Container Runtime"
echo "Would you like to continue? [y/n]"
read USR_ANS

if [ $USR_ANS = "y" ]; then
	INSTALL=true
fi

if [ $INSTALL = true ]; then

    distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
        && curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - \
        && curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

    sudo apt-get update \
        && sudo apt-get install -y nvidia-container-runtime

    echo "Installation complete!"

	ARCH=$(uname -m)
   if [ $ARCH = "x86_64" ]; then
	echo "Would you like to test the installation? [y/n]"
	   read TEST
	   if [ $TEST = "y" ]; then
	      sudo ctr image pull docker.io/nvidia/cuda:11.0-base
            sudo ctr run --rm --gpus 0 -t docker.io/nvidia/cuda:11.0-base cuda-11.0-base nvidia-smi

            echo "Would you like to clean all docker resources?"
            docker system prune -a
	   fi
   fi
fi

