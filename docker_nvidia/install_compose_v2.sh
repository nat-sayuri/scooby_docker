#!/bin/bash

COMPOSE_VERSION=${1:-2.2.3}
INSTALL_COMPOSE=false

echo "Installing Docker Compose v$COMPOSE_VERSION"
echo "Would you like to continue [y/n]?"
read USR_ANS

if [ $USR_ANS = "y" ]; then
	INSTALL=true
fi

if [ $INSTALL = true ]; then

    if ! [ -x "$(command -v docker-compose)" ]; then
        echo "Docker Compose not installed." >&1
        INSTALL_COMPOSE=true
    else
        echo "Docker Compose V1 installed! Verifying version..." >&1
        CURRENT_VERSION=$(docker-compose -v | grep "docker-compose version" | sed 's/docker-compose version \([^-]*\).*/\1/' | cut -c 1-6)
        echo "Docker Compose v$CURRENT_VERSION"
            INSTALL_COMPOSE=true
    fi

    if [ $INSTALL_COMPOSE = true ]; then
        echo "Installing Docker Compose V2..."
        TARGET_ARCH=$(uname -m)
        echo "Target architecture $TARGET_ARCH"
        
	if [ $TARGET_ARCH = "x86_64" ]; then
            DOCKER_CONFIG=${DOCKER_CONFIG:-/usr/local/lib/docker}
            mkdir -p $DOCKER_CONFIG/cli-plugins
            sudo curl -SL "https://github.com/docker/compose/releases/download/v$COMPOSE_VERSION/docker-compose-linux-$TARGET_ARCH" -o $DOCKER_CONFIG/cli-plugins/docker-compose
            sudo chmod +x $DOCKER_CONFIG/cli-plugins/docker-compose

            echo "To run Docker Compose V2 with Visual Studio Code Extension, you must install Compose Switch"
            echo "Would you like to install now? [y/n]"
            read INSTALL_SWITCH
            if [ $INSTALL_SWITCH = "y" ]; then
                sudo curl -fL "https://github.com/docker/compose-switch/releases/download/v1.0.4/docker-compose-linux-amd64" -o /usr/local/bin/compose-switch
                sudo chmod +x /usr/local/bin/compose-switch

                if  [ -x "$(command -v docker-compose)" ]; then
                    echo "Docker Compose V1 installed! Modifying paths and updating alternatives to Compose Switch"
                    sudo mv /usr/local/bin/docker-compose /usr/local/bin/docker-compose-v1
                    sudo update-alternatives --install /usr/local/bin/docker-compose docker-compose /usr/local/bin/docker-compose-v1 1
                fi

                sudo update-alternatives --install /usr/local/bin/docker-compose docker-compose /usr/local/bin/compose-switch 99
                update-alternatives --display docker-compose
            fi 
        elif [ $TARGET_ARCH = "aarch64" ]; then
            
	    DOCKER_CONFIG=${DOCKER_CONFIG:-/usr/local/lib/docker}
            mkdir -p $DOCKER_CONFIG/cli-plugins
            sudo curl -SL "https://github.com/docker/compose/releases/download/v$COMPOSE_VERSION/docker-compose-linux-$TARGET_ARCH" -o $DOCKER_CONFIG/cli-plugins/docker-compose
            sudo chmod +x $DOCKER_CONFIG/cli-plugins/docker-compose

             echo "To run Docker Compose V2 with Visual Studio Code Extension, you must install Compose Switch"
            echo "Would you like to install now? [y/n]"
            read INSTALL_SWITCH
            if [ $INSTALL_SWITCH = "y" ]; then
                sudo curl -fL "https://github.com/docker/compose-switch/releases/download/v1.0.4/docker-compose-linux-arm64" -o /usr/local/bin/compose-switch
                sudo chmod +x /usr/local/bin/compose-switch

                if  [ -x "$(command -v docker-compose)" ]; then
                    echo "Docker Compose V1 installed! Modifying paths and updating alternatives to Compose Switch"
                    sudo mv /usr/local/bin/docker-compose /usr/local/bin/docker-compose-v1
                    sudo update-alternatives --install /usr/local/bin/docker-compose docker-compose /usr/local/bin/docker-compose-v1 1
                fi

                sudo update-alternatives --install /usr/local/bin/docker-compose docker-compose /usr/local/bin/compose-switch 99
                update-alternatives --display docker-compose
            fi 

        else
            echo "Unfortunately target architecture is not configured in current script. Please contact maintainer <natashasayuri@gmail.com> for more information."
        fi
        CURRENT_VERSION=$(docker compose version | grep "Docker Compose version" | sed 's/Docker Compose version \([^-]*\).*/\1/' | cut -c 2-6)
        echo "Docker Compose v$CURRENT_VERSION is succesfully installed!"
    fi
fi

