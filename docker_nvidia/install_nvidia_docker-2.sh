#########################################
# Setting up containerd
#########################################

echo "Setting up Containerd"
echo "Would you like to continue [y/n]?"
read USR_ANS

if [ $USR_ANS = "y" ]; then
	INSTALL=true
fi

if [ $INSTALL = true ]; then

    sudo mkdir -p /etc/containerd \
        && sudo containerd config default | sudo tee /etc/containerd/config.toml

cat <<EOF > containerd-config.patch
--- config.toml.orig    2020-12-18 18:21:41.884984894 +0000
+++ /etc/containerd/config.toml 2020-12-18 18:23:38.137796223 +0000
@@ -94,6 +94,15 @@
        privileged_without_host_devices = false
        base_runtime_spec = ""
        [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
+            SystemdCgroup = true
+       [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.nvidia]
+          privileged_without_host_devices = false
+          runtime_engine = ""
+          runtime_root = ""
+          runtime_type = "io.containerd.runc.v1"
+          [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.nvidia.options]
+            BinaryName = "/usr/bin/nvidia-container-runtime"
+            SystemdCgroup = true
    [plugins."io.containerd.grpc.v1.cri".cni]
    bin_dir = "/opt/cni/bin"
    conf_dir = "/etc/cni/net.d"
EOF

    echo "Restarting Containerd"
    sudo systemctl restart containerd

    echo "Configuration complete!"
    echo "Would you like to test docker? [y/n]"
    read TEST
    if [ $TEST = "y" ]; then
        sudo ctr image pull docker.io/library/hello-world:latest \
        && sudo ctr run --rm -t docker.io/library/hello-world:latest hello-world

        echo "Would you like to clean all docker resources?"
        docker system prune -a
    fi 
fi