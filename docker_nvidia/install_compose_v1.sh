#!/bin/bash

COMPOSE_VERSION=${1:-1.29.2}
INSTALL_COMPOSE=false

echo "Installing Docker Compose v$COMPOSE_VERSION"
echo "Would you like to continue [y/n]?"
read USR_ANS

if [ $USR_ANS = "y" ]; then
	INSTALL=true
fi

if [ $INSTALL = true ]; then

    if ! [ -x "$(command -v docker-compose)" ]; then
        echo "Docker Compose not installed." >&1
        INSTALL_COMPOSE=true
    else
        echo "Docker Compose installed! Verifying version..." >&1
        CURRENT_VERSION=$(docker-compose -v | grep "docker-compose version" | sed 's/docker-compose version \([^-]*\).*/\1/' | cut -c 1-6)
        echo "Docker Compose v$CURRENT_VERSION"

        MINOR_VERSION=$(echo $CURRENT_VERSION | cut -c 3-4)
        if [ "$MINOR_VERSION" -ge 27 ]; then
            echo "Docker Compose version is greater than or equals to 1.27.0." >&1
        else
            echo "Docker Compose version is not compatible to this project. Removing and installing new version." >&1
            apt purge docker-compose
            sudo rm -R /usr/local/bin/docker-compose
            INSTALL_COMPOSE=true
        fi
    fi

    if [ $INSTALL_COMPOSE = true ]; then
        echo "Installing Docker Compose..."
        TARGET_ARCH=$(uname -m)
        echo "Target architecture $TARGET_ARCH"
        if [ $TARGET_ARCH = "x86_64" ]; then
            sudo curl -L "https://github.com/docker/compose/releases/download/$COMPOSE_VERSION/docker-compose-$(uname -s)-$TARGET_ARCH" -o /usr/local/bin/docker-compose
            sudo chmod +x /usr/local/bin/docker-compose
        elif [ $TARGET_ARCH = "aarch64" ]; then
            #requirements
	    sudo apt install python3-pip --reinstall
            sudo apt-get install -y \
                curl \
                python3 \
                python3-pip \
                python-openssl \
                libffi-dev \
                libhdf5-dev \
                zlib1g-dev \
                rustc \
                build-essential \
		python3-testresources

            #install compose
            sudo python3 -m pip install \
                setuptools_rust \
                docker-compose
        else
            echo "Unfortunately target architecture is not configured in current script. Please contact maintainer <natashasayuri@gmail.com> for more information."
        fi
        CURRENT_VERSION=$(docker-compose -v | grep "docker-compose version" | sed 's/docker-compose version \([^-]*\).*/\1/' | cut -c 1-6)
        echo "Docker Compose v$CURRENT_VERSION is succesfully installed!"
    fi
fi

