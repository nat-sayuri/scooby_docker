#########################################
# Script to install Docker
# on Linux X86_64/amd64
# 
# Author: Natasha Nakashima
#
#
# Run the script as sudo!
#
#########################################

echo "After installation, you must reboot your system."
echo "Would you like to continue [y/n]?"
read USR_ANS

if [ $USR_ANS = "y" ]; then
	INSTALL=true
fi

if [ $INSTALL = true ]; then
    echo "Installing Docker..."
	#########################################
	# Removing old versions
	#########################################

	sudo apt-get remove docker docker-engine docker.io containerd runc

	#########################################
	# Set up the repository and install
	#########################################

	sudo apt-get update
	sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
	sudo apt-get update
	sudo apt-get install -y docker-ce docker-ce-cli containerd.io

	#########################################
	# Add your user to the docker group
	#########################################

	sudo usermod -aG docker $USER

	echo Installation conplete!
	echo Would you like to reboot your system [y/n]?
	read USR_REBOOT

	if [ $USR_REBOOT = "y" ]; then
		reboot
	fi
fi


