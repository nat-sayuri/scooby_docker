SCOOBY_DOCKERFILE=$PWD/.devcontainer/ros-foxy
SCOOBY_WS=$PWD/scooby_ws

CURRENT_PATH=$PWD
cd $SCOOBY_DOCKERFILE && \
docker build -t scooby-docker:ros-dev --build-arg USER_NAME=samsung --build-arg USER_PASSWORD=samsung --build-arg USER_UID=1000 --build-arg USER_GID=1000 --build-arg NVIDIA_GPU=yes --build-arg CUDA_MAJOR=11 --build-arg CUDA_MINOR=1 -f $SCOOBY_DOCKERFILE/Dockerfile . 

cd $CURRENT_PATH
docker volume create --driver=local --opt type=none --opt o=bind --opt device=$SCOOBY_WS scooby-volume 
docker run -it --runtime=nvidia --net=host --workdir=/samsung/scooby_ws --user=samsung -e ROS_DOMAIN_ID=200 -e DISPLAY=$DISPLAY --env NVIDIA_VISIBLE_DEVICES=all -v $HOME/.Xauthority:/root/.Xauthority -v /tmp/.X11-unix/:/tmp/.X11-unix -e scooby-volume:/samsung/scooby_ws --name scooby-development scooby-docker:ros-dev
